import java.util.*;

/**
 * Created by rahul on 2/17/16.
 */
public class SortUtil<T> {
    private static int SUBLIST_SIZE_BEFORE_INSERTION_SORT = 2;
    public static <T extends Comparable> void mergesort(ArrayList<T> array, Comparator<? super T> comparator){
        mergeSortRecursive(array, 0, array.size()-1);
    }

    public static <T extends Comparable> void quicksort(ArrayList<T> array, Comparator<? super T> comparator){

    }

    private static <T extends Comparable> void mergeSortRecursive(ArrayList<T> array, int left, int right){
        if(left>right) {
            return;
        }
        //int midpoint = (left+right)/2;
        int midpoint = left + (right - left)/2;
        if((right-left+1)<=SUBLIST_SIZE_BEFORE_INSERTION_SORT){
            insertionSort(array, left, midpoint, right);
            System.out.println("Insertion sort");
            return;
        }
        mergeSortRecursive(array, left, midpoint);
        mergeSortRecursive(array, midpoint + 1, right);
        merge(array, left, midpoint, right);
    }

    private static <T extends Comparable> void insertionSort(ArrayList<T> array, int left, int mid, int right){
        //Insertion Sort Sublists
        for(int i = left; i < mid; i++){
            for(int n = 1; n < (mid-left)+1; n++){
                T key = array.get(n);
                int j = n-1;
                while(j>=0 && array.get(j).compareTo(key)>0){
                    array.set(j+1, array.get(j));
                    j--;
                }
                array.set(j+1, key);
            }
        }
        for(int i = mid; i < right; i++){
            for(int n = 1; n < (right-mid)+1; n++){
                T key = array.get(n);
                int j = n-1;
                while(j>=0 && array.get(j).compareTo(key)>0){
                    array.set(j+1, array.get(j));
                    j--;
                }
                array.set(j+1, key);
            }
        }
        /*
        System.out.println("LIST RUNNING:");
        for(T e : array){
            System.out.println(e);
        }
        */
        System.out.println("Insertion Sort complete from index " + left + " to " + right);
    }
    private static <T extends Comparable> void merge(ArrayList<T> array, int left, int mid, int right){
        ArrayList<T> temp = new ArrayList<>(right-left+1);
        for(int i = left; i <= right; i++){
            temp.add(i, array.get(i));
        }
        int i1 = left;
        int i2 = mid + 1;
        int i3 = left;

        while (i1 <= mid && i2 <= right) {
            if (temp.get(i1).compareTo(temp.get(i2))<=0) {
                array.set(i3, temp.get(i1));
                i1++;
            } else {
                array.set(i3, temp.get(i2));
                i2++;
            }
            i3++;
        }
        while (i1 <= mid) {
            array.set(i3, temp.get(i1));
            i3++;
            i1++;
        }
        //Old code
        /*
        int i1 = left;
        int i2 = mid;
        int idx = -1000;
        //System.out.println(idx++);
        while(i1 < mid && i2 < right){
            if(array.get(i1).compareTo(array.get(i2))<0){
                temp.add(array.get(i1));
                i1++;
            }
            else{
                temp.add(array.get(i2));
                i2++;
            }
            System.out.println(idx++);
        }
        while(i2 < right){
            temp.add(array.get(i2));
            i2++;
            System.out.println(idx++);
        }
        while(i1 < mid){
            temp.add(array.get(i1));
            i1++;
        }
        for(int i = 0; i < temp.size(); i++){
            System.out.println("DEBUG " + i);
            array.set(left+i, temp.get(i));
        }
        for(T e : temp){
            System.out.println(e);
        }
        */
    }

    public static ArrayList<Integer> generateBestCase(int size){
        ArrayList<Integer> output = new ArrayList<>(size);
        for(int i = 0; i < size; i++){
            output.add(i);
        }
        return output;
    }

    public static ArrayList<Integer> generateAverageCase(int size){
        long seed = 2420;
        Random generator = new Random(seed);
        ArrayList<Integer> output = new ArrayList<>(size);

        for(int i = 0; i < size; i++){
            int idx = 0;
            while(true){
                int value = generator.nextInt(size);
                if (!output.contains(value)) {
                    output.add(i, value);
                    break;
                }

            }
        }
        return output;

    }

    public static ArrayList<Integer> generateWorstCase(int size){
        ArrayList<Integer> bestCase = generateBestCase(size);
        ArrayList<Integer> output = new ArrayList<>(size);
        int j = (size-1);
        for(int i = 0; i < size; i++){
            output.add(i, bestCase.get(j));
            j--;
        }
        return output;
    }

    public static void main(String[] args){
        int size = 4;
        ArrayList<Integer> testBest = generateBestCase(size);
        ArrayList<Integer> testAvg = generateAverageCase(size);
        ArrayList<Integer> testWorst = generateWorstCase(size);
        ArrayList<ArrayList<Integer>> setOfSets = new ArrayList<>(Arrays.asList(testBest, testAvg, testWorst));
        mergesort(testAvg, Integer::compareTo);

        for(ArrayList<Integer> e : setOfSets){
            System.out.println("\nList:");
            for(int i = 0; i < size; i++){
                System.out.println(e.get(i));
            }
        }
    }
    public static void setSublistSizeBeforeInsertionSort(int size){
        SUBLIST_SIZE_BEFORE_INSERTION_SORT = size;
    }
}

